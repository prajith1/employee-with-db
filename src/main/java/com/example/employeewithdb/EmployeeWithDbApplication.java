package com.example.employeewithdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeWithDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeWithDbApplication.class, args);
    }

}

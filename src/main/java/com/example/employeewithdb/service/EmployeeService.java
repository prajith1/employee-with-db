package com.example.employeewithdb.service;

import com.example.employeewithdb.entity.EmployeeEntity;
import com.example.employeewithdb.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    EmployeeService(EmployeeRepository employeeRepository){
            this.employeeRepository = employeeRepository;
    }

    public EmployeeEntity createEmployee(EmployeeEntity employee) {
        return employeeRepository.save(employee);
    }
    public List<EmployeeEntity> getEmployees(){
       return employeeRepository.findAll();
    }
}

package com.example.employeewithdb.controller;

import com.example.employeewithdb.entity.EmployeeEntity;
import com.example.employeewithdb.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController{
    private final EmployeeService employeeService;

    @Autowired
    EmployeeController(EmployeeService employeeService){
        this.employeeService = employeeService;
    }

    @PostMapping(produces = "application/json")
    public ResponseEntity<EmployeeEntity> createEmployee(@RequestBody EmployeeEntity employee)  {
        EmployeeEntity createdEmployeeEntity = employeeService.createEmployee(employee);
        return  new ResponseEntity<>(createdEmployeeEntity, HttpStatus.CREATED);
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<EmployeeEntity>> getEmployees(){
         List<EmployeeEntity> employees = employeeService.getEmployees();

        if (!employees.isEmpty())
            return new ResponseEntity<>(employees, HttpStatus.OK);

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}

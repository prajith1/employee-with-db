package com.example.employeewithdb.entity;
import lombok.Data;
import javax.persistence.*;

@Entity
@Data
@Table(name = "Employees")
public class EmployeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Name")
    private String name;

    @Column(name = "Gender")
    private String gender;

    @Column(name = "City")
    private String city;

    @Column(name = "Age")
    private Integer age;

}